-- Base
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

 -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myEmojiFont :: String
myEmojiFont = "xft:JoyPixels:regular:size=9:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask        -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "alacritty"    -- Sets default terminal

myBrowser :: String
myBrowser = "qutebrowser "  -- Sets qutebrowser as browser

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs' "  -- Makes emacs keybindings easier to type

myEditor :: String
myEditor = "emacsclient -c -a 'emacs' "  -- Sets emacs as editor
-- myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor

myBorderWidth :: Dimension
myBorderWidth = 4           -- Sets border width for windows

myNormColor :: String
myNormColor  = "#3B4252"

myFocusColor :: String
myFocusColor = "#88C0D0"

altMask :: KeyMask
altMask = mod1Mask          -- Setting this for use in xprompts

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook = do
    spawnOnce "lxsession &"
    spawnOnce "/usr/bin/emacs --daemon &" -- emacs daemon for the emacsclient
    -- spawnOnce "kak -d -s mysession &"  -- kakoune daemon for better performance
    -- spawnOnce "urxvtd -q -o -f &"      -- urxvt daemon for better performance

    -- spawnOnce "/bin/ls ~/wallpapers | shuf -n 1 | xargs xwallpaper --stretch"  -- set random xwallpaper
    -- spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
--    spawnOnce "feh --randomize --bg-fill ~/Pictures/wallpapers/*"  -- feh set random wallpaper
    -- spawnOnce "nitrogen --restore &"   -- if you prefer nitrogen to feh
    setWMName "LG3D"

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "ncmpcpp" spawnNcmpcpp findNcmpcpp manageNcmpcpp
                , NS "calculator" spawnCalc findCalc manageCalc
                , NS "terfile" spawnVifm findVifm manageVifm
                , NS "mixer" spawnPulsemixer findPulsemixer managePulsemixer
                , NS "file" spawnPcmanfm findPcmanfm managePcmanfm
                ]
  where
    spawnTerm  = myTerminal ++ " -t scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.4
                 w = 1
                 t = 0
                 l = 0
    spawnNcmpcpp  = myTerminal ++ " -t ncmpcpp -e ncmpcpp"
    findNcmpcpp   = title =? "ncmpcpp"
    manageNcmpcpp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.7
                 w = 0.7
                 t = 0.85 -h
                 l = 0.85 -w
    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w
    spawnVifm  = myTerminal ++ " -t vifmrun -e vifmrun"
    findVifm   = title =? "vifmrun"
    manageVifm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.05
                 l = 0.5
    spawnPulsemixer  = myTerminal ++ " -t pulsemixer -e pulsemixer"
    findPulsemixer   = title =? "pulsemixer"
    managePulsemixer = customFloating $ W.RationalRect l t w h
               where
                 h = 0.7
                 w = 0.7
                 t = 0.85 -h
                 l = 0.85 -w

    spawnPcmanfm  = "pcmanfm"
    findPcmanfm   = className =? "Pcmanfm"
    managePcmanfm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.6
                 w = 0.5
                 t = 0.8 -h
                 l = 0.75 -w

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 3
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ mySpacing 3
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ windowNavigation
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ mySpacing 3
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#d4be98"
                 , inactiveColor       = "#282828"
                 , activeBorderColor   = "#d3869b"
                 , inactiveBorderColor = "#282828"
                 , activeTextColor     = "#d4be98"
                 , inactiveTextColor   = "#3c3836"
                 }

myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     tall
                                 ||| magnify
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| floats

-- myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
myWorkspaces = [ " dev "," www ", " test ", " sys ", " doc ", " mail ", " chat ", " med ", " game "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , title =? "Mozilla Firefox"   --> doShift ( myWorkspaces !! 1 )
     , className =? "Brave-browser" --> doShift ( myWorkspaces !! 1 )
     , className =? "qutebrowser"   --> doShift ( myWorkspaces !! 1 )
     , className =? "Postman"   --> doShift ( myWorkspaces !! 2 )
     , className =? "Libre Office"--> doShift ( myWorkspaces !! 4 )
     , className =? "Microsoft Teams"--> doShift ( myWorkspaces !! 5 )
     , className =? "Mailspring"   --> doShift ( myWorkspaces !! 5 )
     , className =? "discord"	 --> doShift ( myWorkspaces !! 6 )
     , className =? "TelegramDesktop"   --> doShift ( myWorkspaces !! 6 )
     , className =? "whatsapp-nativefier-d40211"   --> doShift ( myWorkspaces !! 6 )
     , className =? "zoom"	 --> doShift ( myWorkspaces !! 6 )
     , className =? "mpv"           --> doShift ( myWorkspaces !! 7 )
     , className =? "vlc"           --> doShift ( myWorkspaces !! 7 )
     , className =? "Steam"	 --> doShift ( myWorkspaces !! 8 )
     ] <+> namedScratchpadManageHook myScratchPads

myKeys :: [(String, X ())]
myKeys =
    -- Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")                        -- Recompiles xmonad
        , ("M-S-r", spawn "xmonad --restart")                          -- Restarts xmonad
        , ("M-S-q", io exitSuccess)  -- Quits xmonad

    -- Run Prompt
        , ("M-S-p", spawn "dmenu_run -p \"Run: \"") -- Dmenu
        , ("M-r", spawn "rofi -no-lazy-grab -show drun -modi run,drun,window -theme $HOME/.config/rofi/launcher/style -drun-icon-theme \"candy-icons\" ") -- Dmenu
 	-- Other Dmenu Prompts
    -- In Xmonad and many tiling window managers, M-p is the default keybinding to
    -- launch dmenu_run, so I've decided to use M-p plus KEY for these dmenu scripts.
        , ("M-p a", spawn "dm-sounds")    -- choose an ambient background
        , ("M-p c", spawn "dm-colpick")   -- pick color from our scheme
        , ("M-p d", spawn "displayselect") -- select display
        , ("M-p e", spawn "dm-confedit")  -- edit config files
        , ("M-p i", spawn "maimpick")      -- screenshots (images)
        , ("M-p k", spawn "dm-kill")      -- kill processes
        , ("M-p n", spawn "dm-man")       -- manpages
        , ("M-p o", spawn "dm-bookman")   -- qutebrowser bookmarks/history
        , ("M-p p", spawn "passmenu")     -- passmenu
        , ("M-p q", spawn "dm-logout")    -- logout menu
        , ("M-p r", spawn "dm-reddit")    -- reddio (a reddit viewer)
        , ("M-p s", spawn "dm-websearch") -- search various search engines
        , ("M-p m", spawn "dmenumount") -- Mount a Usb
        , ("M-p u", spawn "dmenuumount") -- Umount a Usb
        , ("M-p t", spawn "td-toggle") -- Toggle transmision demon
        , ("M-p j", spawn "dmenuunicode") -- Toggle transmision demon
        , ("M-p w", spawn "dm-wifi") -- Toggle transmision demon

       -- , ("M-t p", spawn "alacritty -e pulsemixer") --Audio
        , ("M-t t", spawn "torwrap") --Torrent
        , ("M-t h", spawn "alacritty -e htop")
        , ("M-t u", spawn "alacritty -e ~/.local/bin/statusbar/sb-popupgrade") --Upgrade
       -- , ("M-t m", spawn "alacritty -e ncmpcpp") --Music
--        , ("M-t r", spawn "alacritty -e vifmrun") --File manager
        , ("M-t n", spawn "alacritty -e newsboat") --RSS reader
        , ("M-t w", spawn "alacritty -e nmtui") --RSS reader


        -- Useful programs to have a keybinding for launch
        , ("M-<Return>", spawn (myTerminal))

        , ("M-S-c", kill1)     -- Kill the currently focused client
        , ("M-S-a", killAll)   -- Kill all windows on current workspace

   -- Workspaces
        , ("M-.", nextScreen)  -- Switch focus to next monitor
        , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
        , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

 -- Floating windows
        , ("M-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
        , ("M-g", withFocused $ windows . W.sink)  -- Push floating window back to tile
        , ("M-S-g", sinkAll)                       -- Push ALL floating windows to tile

    -- Increase/decrease spacing (gaps)
        , ("M-d", decWindowSpacing 4)           -- Decrease window spacing
        , ("M-i", incWindowSpacing 4)           -- Increase window spacing
        , ("M-S-d", decScreenSpacing 4)         -- Decrease screen spacing
        , ("M-S-i", incScreenSpacing 4)         -- Increase screen spacing

    -- Windows navigation
        , ("M-m", windows W.focusMaster)  -- Move focus to the master window
        , ("M-j", windows W.focusDown)    -- Move focus to the next window
        , ("M-k", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
        , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
        , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
        , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

    -- Layouts
        , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
        , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

 -- Increase/decrease windows in the master pane or the stack
        , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase number of clients in master pane
        , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease number of clients in master pane
        , ("M-C-<Up>", increaseLimit)                   -- Increase number of windows
        , ("M-C-<Down>", decreaseLimit)                 -- Decrease number of windows

    -- Window resizing
        , ("M-h", sendMessage Shrink)                   -- Shrink horiz window width
        , ("M-l", sendMessage Expand)                   -- Expand horiz window width
        , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)          -- Exoand vert window width

    -- Sublayouts
    -- This is used to push windows to tabbed sublayouts, or pull them out of it.
        , ("M-C-h", sendMessage $ pullGroup L)
        , ("M-C-l", sendMessage $ pullGroup R)
        , ("M-C-k", sendMessage $ pullGroup U)
        , ("M-C-j", sendMessage $ pullGroup D)
        , ("M-C-m", withFocused (sendMessage . MergeAll))
        -- , ("MkC-u", withFocused (sendMessage . UnMerge))
        , ("M-C-/", withFocused (sendMessage . UnMergeAll))
        , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
        , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab
-- Scratchpads
    -- Toggle show/hide these programs.  They run on a hidden workspace.
    -- When you toggle them to show, it brings them to your current workspace.
    -- Toggle them to hide and it sends them back to hidden workspace (NSP).
        , ("M-o t", namedScratchpadAction myScratchPads "terminal")
        , ("M-t m", namedScratchpadAction myScratchPads "ncmpcpp")
        , ("M-o c", namedScratchpadAction myScratchPads "calculator")
        , ("M-t r", namedScratchpadAction myScratchPads "terfile")
        , ("M-t p", namedScratchpadAction myScratchPads "mixer")
        , ("M-o r", namedScratchpadAction myScratchPads "file")


       -- , ("M-o r", spawn "pcmanfm")
        , ("M-o l", spawn "libreoffice")
        , ("M-o m", spawn "mailspring")
        , ("M-o w", spawn "mpv --no-cache --no-osc --no-input-default-bindings --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)")


        , ("M-c", spawn "crontog")

        -- Controls for mocp music player (SUPER-u followed by a key)
        , ("M-u p", spawn "mpc play")
        , ("M-u l", spawn "mpc next")
        , ("M-u h", spawn "mpc previous")
        , ("M-u <Space>", spawn "mpc toggle")

        , ("M-e", spawn "neovide")  

        -- Multimedia Keys
        , ("<XF86AudioPlay>", spawn ("mpc toggle"))
        , ("<XF86AudioPuse>", spawn ("mpc pause"))
        , ("<XF86AudioPrev>", spawn ("mpc prev"))
        , ("<XF86AudioNext>", spawn ("mpc next"))
        , ("<XF86AudioMute>",   spawn "amixer set Master toggle")
        , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
        , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
        , ("<XF86MonBrightnessUp>", spawn "xbacklight -inc 5")
        , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 5")
        , ("M--", spawn "amixer set Master 5%- unmute")
        , ("M-=", spawn "amixer set Master 5%+ unmute")
        , ("<XF86HomePage>", spawn "firefox")
        , ("<XF86Search>", safeSpawn "firefox" ["https://www.duckduckgo.com/"])
        , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
        , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
        , ("<XF86Eject>", spawn "toggleeject")
        , ("<Print>", spawn "maim -s | xclip -selection clipboard -t image/png")
        , ("S-<Print>", spawn "maimpick")
        , ("M-<Insert>", spawn "notify-send \"📋 Clipboard contents:\" \"$(xclip -o -selection clipboard)\"")
        ]
-- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

main :: IO ()
main = do
    -- Launching three instances of xmobar on their monitors.
    xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc0"
    xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/xmobarrc2"
    -- the xmonad, ya know...what the WM is named after!
    xmonad $  ewmh def

        { manageHook         = myManageHook <+> manageDocks
        , handleEventHook    = docksEventHook
                               -- Uncomment this line to enable fullscreen support on things like YouTube/Netflix.
                               -- This works perfect on SINGLE monitor systems. On multi-monitor systems,
                               -- it adds a border around the window if screen does not have focus. So, my solution
                               -- is to use a keybinding to toggle fullscreen noborders instead.  (M-<Space>)
                               <+> fullscreenEventHook

        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc0 x
                                        >> hPutStrLn xmproc1 x                          -- xmobar on monitor 2
                        , ppCurrent = xmobarColor "#7daea3" "" . wrap "[" "]"           -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#928374" "" . clickable              -- Visible but not current workspace
                        , ppHidden = xmobarColor "#fbf1c7" "" . wrap "" "" . clickable -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#3c3836" ""  . clickable     -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#89b482" "" . shorten 45               -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separators in xmobar
                        , ppUrgent = xmobarColor "#98971a" "" . wrap "!" "!"            -- Urgent workspace
                        , ppExtras  = [windowCount]                                     -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
        -- , logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
        --                 { ppOutput = \x -> hPutStrLn xmproc0 x
        --                                 >> hPutStrLn xmproc1 x                          -- xmobar on monitor 2
        --                 , ppCurrent = xmobarColor "#5E81AC" "" . wrap "[" "]"           -- Current workspace in xmobar
        --                 , ppVisible = xmobarColor "#9099AB" "" . clickable              -- Visible but not current workspace
        --                 , ppHidden = xmobarColor "#D8DEE9" "" . wrap "" "" . clickable -- Hidden workspaces in xmobar
        --                 , ppHiddenNoWindows = xmobarColor "#3B4252" ""  . clickable     -- Hidden workspaces (no windows)
        --                 , ppTitle = xmobarColor "#8FBCBB" "" . shorten 45               -- Title of active window in xmobar
        --                 , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separators in xmobar
        --                 , ppUrgent = xmobarColor "#A3BE8C" "" . wrap "!" "!"            -- Urgent workspace
        --                 , ppExtras  = [windowCount]                                     -- # of windows current workspace
        --                 , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
        --                 }
        } `additionalKeysP` myKeys
