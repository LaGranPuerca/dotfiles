//Modify this file to change what commands output to your statusbar, and recompile using the make command.

static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
 	{"", "sb-kbselect", 0, 30},
	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	9},
//	{"",	"sb-tasks",	10,	26},
	{"",	"/home/dave/.local/bin/statusbar/sb-music",	0,	11},
	{"",	"/home/dave/.local/bin/statusbar/sb-pacpackages",	0,	8},
	{"",	"/home/dave/.local/bin/statusbar/sb-news",		0,	6},
//	{"",	"sb-mailbox",	180,	12},
//	 {"",	"sb-price lbc \"LBRY Token\" 📚",			9000,	22},
//	 {"",	"sb-price bat \"Basic Attention Token\" 🦁",	9000,	20},
//	 {"",	"sb-price link \"Chainlink\" 🔗",			300,	25},
//	 {"",	"sb-price xmr \"Monero\" 🔒",			9000,	24},
	{"",	"/home/dave/.local/bin/statusbar/sb-price eth Ethereum 🍸",	1860,	23},
	{"",	"/home/dave/.local/bin/statusbar/sb-price btc Bitcoin 💰",	1860,	21},
	{"",	"/home/dave/.local/bin/statusbar/sb-torrent",	20,	7},
	{"",	"/home/dave/.local/bin/statusbar/sb-memory",	10,	14},
	{"",	"/home/dave/.local/bin/statusbar/sb-cpu",	10,	18},
	{"",	"/home/dave/.local/bin/statusbar/sb-disk",	0,	13},
	{"",	"/home/dave/.local/bin/statusbar/sb-nettraf",	1,	16},
	{"",	"/home/dave/.local/bin/statusbar/sb-moonphase",	18000,	17},
	{"",	"/home/dave/.local/bin/statusbar/sb-forecast",	10800,	5},
	{"",	"/home/dave/.local/bin/statusbar/sb-clock",	5,	1},
	{"",	"/home/dave/.local/bin/statusbar/sb-volume",	0,	10},
	{"",	"/home/dave/.local/bin/statusbar/sb-internet",	5,	4},
	{"",	"/home/dave/.local/bin/statusbar/sb-battery",	5,	3},
//	{"",	"sb-help-icon",	0,	15},
};
//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = " ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:
