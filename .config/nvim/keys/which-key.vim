" Map leader to which_key

nnoremap <silent> <leader> :silent WhichKey '<Space>'<CR>

vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual '<Space>'<CR>

" Create map to add keys to

let g:which_key_map =  {}

" Define a separator

let g:which_key_sep = ' '

" set timeoutlen=100

" Not a fan of floating windows for this
let g:which_key_use_floating_win = 0
" Change the colors if you want
highlight default link WhichKey          Operator
highlight default link WhichKeySeperator Comment
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function
" Hide status line
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler
" Single mappings
"
let g:which_key_map['/'] = [ '<Plug>NERDCommenterToggle'  , 'comment' ]
let g:which_key_map['s']= {
            \'name': '+split' ,
            \'h' : [':vsp'     , 'horizontal split']          ,
            \'v' : [':sp'     , 'vertical split']         
          \ }
let g:which_key_map['w'] = {
      \ 'name' : '+windows' ,
      \ 'w' : ['<C-W>w'     , 'other-window']          ,
      \ 'c' : ['q'          , 'close-window']          ,
      \ 'd' : ['<C-W>c'     , 'delete-window']         ,
      \ 'h' : ['<C-W>h'     , 'window-left']           ,
      \ 'j' : ['<C-W>j'     , 'window-below']          ,
      \ 'l' : ['<C-W>l'     , 'window-right']          ,
      \ 'k' : ['<C-W>k'     , 'window-up']             ,
      \ 'H' : ['<C-W>5<'    , 'expand-window-left']    ,
      \ 'J' : [':resize +5'  , 'expand-window-below']   ,
      \ 'L' : ['<C-W>5>'    , 'expand-window-right']   ,
      \ 'K' : [':resize -5'  , 'expand-window-up']      ,
      \ '=' : ['<C-W>='     , 'balance-window']        ,
      \ }
let g:which_key_map.b = {
      \ 'name' : '+buffer' ,
      \ '1' : ['b1'        , 'buffer 1']        ,
      \ '2' : ['b2'        , 'buffer 2']        ,
      \ '3' : ['b3'        , 'buffer 3']        ,
      \ '4' : ['b4'        , 'buffer 4']        ,
      \ '5' : ['b5'        , 'buffer 5']        ,
      \ 'd' : ['bd'        , 'delete-buffer']   ,
      \ 'f' : ['bfirst'    , 'first-buffer']    ,
      \ 'h' : ['Startify'  , 'home-buffer']     ,
      \ 'l' : ['blast'     , 'last-buffer']     ,
      \ 'n' : ['bnext'     , 'next-buffer']     ,
      \ 'p' : ['bprevious' , 'previous-buffer'] ,
      \ 'B' : ['Buffers'   , 'fzf-buffer']      ,
      \ }
let g:which_key_map.o = {
      \ 'name' : '+open' ,
      \ 't' : [':vsp<CR>:terminal<CR>i' , 'open terminal'] ,
      \ 'T' : [':sp<CR>15<C-w>-:terminal<CR>i'   , 'open terminal in window']      ,
      \ 'e' : [':CocCommand explorer'   , 'open explorer']      ,
      \ 'f' : [':CocCommand explorer --prset floating<CR>'   , 'open explorer in floating window']      ,
      \ }
let g:which_key_map.c = {
      \ 'name' : '+coc' ,
      \ }
let g:which_key_map.f = {
      \ 'name' : '+file' ,
      \ 'e' : ['<space>fe :vsp $MYVIMRC<cr>' , 'open vimrc'] ,
      \ 'r' : ['<space>fr :so $MYVIMRC<cr>'   , 'reload vimrc']      ,
      \ }
let g:which_key_map.z = {
      \ 'name' : '+fzf' ,
      \ }
" Register which key map
call which_key#register('<Space>', "g:which_key_map")
