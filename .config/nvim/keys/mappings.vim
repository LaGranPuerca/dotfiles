inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <c-space> coc#refresh()
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd :vsp<CR><Plug>(coc-definition)zz
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

"snippets
" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand-jump)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-;> <Plug>(coc-snippets-expand)

" Use <leader>cs for convert visual selected code to snippet
xmap <leader>cs  <Plug>(coc-convert-snippet)

inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()



nmap <C-p> :FZFGitIgnore <CR>
imap <C-p> <Esc>:FZFGitIgnore <CR>
nmap <C-B> :Buffers <CR>
nnoremap <C-L> :Lines <CR>
nmap <Leader>zl <Esc>:Lines<CR>
nmap <Leader>zb <Esc>:Buffers<CR>
nmap <Leader>zc <Esc>:Commits<CR>

nmap <C-p> :Buffer<CR>

" replace yanked word
" binds: cpw, cpi(
nnoremap <silent> cp :let g:substitutemotion_reg = v:register
            \ <bar> set opfunc=SubstituteMotion<CR>g@

function! SubstituteMotion(type, ...)
	let l:reg = g:substitutemotion_reg
	if a:0
		silent exe "normal! `<" . a:type . "`>\"_c\<c-r>" . l:reg . "\<esc>"
	elseif a:type == 'line'
		silent exe "normal! '[V']\"_c\<c-r>" . l:reg . "\<esc>"
	elseif a:type == 'block'
		silent exe "normal! `[\<C-V>`]\"_c\<c-r>" . l:reg . "\<esc>"
	else
		silent exe "normal! `[v`]\"_c\<c-r>" . l:reg . "\<esc>"
	endif
endfunction


set fillchars+=vert:\|
" go normal mode
imap <silent> jk <Esc>
imap <silent> kj <Esc>

" save
nmap <c-s> :w<CR>
nnoremap <space>fs :w <cr>
imap <c-s> <Esc>:w<CR>a

" quit
nmap <c-q> :q<CR>
imap <c-q> <Esc>:q<CR>

" move line up and down
nnoremap <S-Up> :m-2<CR>
nnoremap <S-Down> :m+<CR>
inoremap <S-Up> <Esc>:m-2<CR>
inoremap <S-Down> <Esc>:m+<CR>
"add split
nnoremap<space>sv :vsp <CR>
nnoremap<space>sh :sp <CR>

" move between split views
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <space>wc :q <cr>
nnoremap <space>wk <C-w>k
nnoremap <space>wj <C-w>j
nnoremap <space>wh <C-w>h
nnoremap <space>wl <C-w>l
nnoremap <space>ww <C-w>w
nnoremap <space>wd <C-w>c
nnoremap <space>wH <C-w>5<
nnoremap <space>wJ :resize -5<cr>
nnoremap <space>wK :resize +5<cr>
nnoremap <space>wL <C-w>5>
nnoremap <space>w= <C-w>=

" auto close bracket
" inoremap {<cr> {<cr>}<c-o>O
" inoremap {<cr> {<cr>}<c-o>O
" inoremap [<cr> [<cr>]<c-o>O<tab>
" inoremap (<cr> (<cr>)<c-o>O<tab>

"Buffers
nnoremap <space>bn :bn <cr>
nnoremap <space>bp :bp <cr>
nnoremap <space>bb :Buffers <cr>
nnoremap <space>bd :bd <cr>
nnoremap <space>b1 :b1 <cr>
nnoremap <space>b2 :b2 <cr>
nnoremap <space>b3 :b3 <cr>
nnoremap <space>b4 :b4 <cr>
nnoremap <space>b5 :b5 <cr>
nnoremap <space>bf :bfirst <cr>


" spell ckeck
map <F6> :setlocal spell! spelllang=en_us<CR>

" select functin
nnoremap <Leader>vf va{V

" switch to normal mode in terminal
tnoremap <Esc> <C-\><C-n>
tnoremap jk <C-\><C-n>

" open vimrc
nnoremap <space>fe :vsp $MYVIMRC<cr>
" reload the .vimrc
nnoremap <space>fr :so $MYVIMRC<cr>

" open 
nnoremap <space>oT :vsp<CR>:terminal<CR>i
nnoremap <space>ot :sp<CR>15<C-w>-:terminal<CR>i

nnoremap <space>/ :<Plug>NERDCommenterToggle



" remove & change the word under the cursor
nnoremap c* *Ncgn
nnoremap d* *Ndgn

