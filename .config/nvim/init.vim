      "     ___       __________   __________ #
      "    /  /      /  _______/  /  ____   / #
      "   /  /      /  / _____   /  /___/  /  #
      "  /  /      /  / /    /  /   ______/   #
      " /  /_____ /  /___/  /  /   /          #
      " \_______/ \________/  /___/           #
      "
source $HOME/.config/nvim/vim-plug/pluggins.vim
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim
source $HOME/.config/nvim/themes/gruvbox-material.vim
source $HOME/.config/nvim/keys/which-key.vim

source $HOME/.config/nvim/plug-config/coc.vim
luafile $HOME/.config/nvim/lua/plug-colorizer.lua
source $HOME/.config/nvim/plug-config/start-screen.vim
