
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'roxma/nvim-yarp'

Plug 'neoclide/coc.nvim', {'branch': 'release'}


" adds the status line when something is being processed in the background
let g:airline#extensions#coc#enabled = 1

command! -nargs=0 Prettier :CocCommand prettier.formatFile
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

command! -nargs=0 Format 	:call 		CocAction('format')
command! -nargs=? Fold 		:call 		CocAction('fold', <f-args>)
command! -nargs=0 OR   		:call		CocAction('runCommand', 'editor.action.organizeImport')

Plug 'pangloss/vim-javascript'


Plug 'ianks/vim-tsx'


Plug 'leafgarland/typescript-vim'


Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" ignore files and folders in git ignore
command! FZFGitIgnore call fzf#run(fzf#wrap({'source': 'git ls-files --exclude-standard --others --cached'}))

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme = "gruvbox_material"
let g:airline#extensions#tabline#enabled = 1
let g:airline_detect_modified=1
let g:airline_powerline_fonts = 1
let g:airline_disable_statusline = 0

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ':'
let g:airline_symbols.colnr = ':'
let g:airline_symbols.maxlinenr = ' '
let g:airline#extensions#coc#enabled = 1
let airline#extensions#coc#error_symbol = 'E:'
let airline#extensions#coc#warning_symbol = 'W:'
let g:airline#extensions#whitespace#symbol = ':'
let airline#extensions#coc#stl_format_err = '%E{[%e(#%fe)]}'
let airline#extensions#coc#stl_format_warn = '%W{[%w(#%fw)]}'
let g:airline#extensions#wordcount#enabled = 0


Plug 'tpope/vim-surround'

Plug 'terryma/vim-multiple-cursors'

Plug 'mattn/emmet-vim'


Plug 'Raimondi/delimitMate'

Plug 'scrooloose/nerdcommenter'
" Create default mappings
let g:NERDCreateDefaultMappings = 0
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDAltDelims_java = 1
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1


Plug 'cometsong/CommentFrame.vim'

Plug 'vimwiki/vimwiki'
let g:vimwiki_list = [{'path': '~/workspace/wiki',  'syntax': 'markdown', 'ext': '.md'}]


Plug '907th/vim-auto-save'
let g:auto_save = 0
let g:auto_save_events = ["InsertLeave", "TextChanged"]


Plug 'wincent/scalpel'


Plug 'jiangmiao/auto-pairs'


Plug 'psliwka/vim-smoothie'


Plug 'ryanoasis/vim-devicons'


Plug 'unblevable/quick-scope'


Plug 'drewtempelmeyer/palenight.vim'
Plug 'joshdick/onedark.vim'
Plug 'arcticicestudio/nord-vim'

Plug 'nvim-treesitter/nvim-treesitter'

Plug 'liuchengxu/vim-which-key'
Plug 'sainnhe/gruvbox-material'

Plug 'norcalli/nvim-colorizer.lua'

Plug 'mhinz/vim-startify'

Plug 'fxn/vim-monochrome'
call plug#end()
